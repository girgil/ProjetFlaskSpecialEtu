from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
app = Flask(__name__)
Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] =True
app.config['SECRET_KEY']="8d939a9d-2d7e-4261-b440-4d8706877b83"
login_manager = LoginManager(app)
login_manager.login_view = "login"
import os.path
def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),p
        )
    )

from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']=(
    'sqlite:///'+mkpath('../myapp.db'))

db = SQLAlchemy(app)