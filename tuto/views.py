from .app import app, db
from flask import render_template, url_for, redirect, request
from .models import *
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField
from wtforms.validators import DataRequired
from hashlib import sha256
from flask_login import UserMixin, login_user, current_user, logout_user, login_required

@app.route("/")
def home():
    return render_template(
        "home.html",
        title="Hello world avec template",
        names=["Pierre","Paul","Jacques"]
    ) 

#Exercice 7
@app.route("/books")
def books():
    return render_template(
        "books.html",
        title = 'Liste de livres',
        books = get_sample_fromBD()
    )

@app.route("/books/get_books_between_5_20")
def books_between_5_20():
    return render_template(
        "get_books_between_5_20.html",
        title = "Liste des livres avec un prix entre 5 et 20€",
        books = get_books_between_5_20()
    )

@app.route("/books/tolkien")
def books_tolkien():
    return render_template(
        "books.html",
        title = "Livres de Tolkien",
        books = get_books_from_Tolkien()
    )

class AuthorForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom', validators=[DataRequired()])


@app.route("/edit/author/<int:id>")
@login_required
def edit_author(id):
    a = get_author(id)
    f = AuthorForm(id=a.id,name=a.name)
    return render_template("edit-author.html",title="Edition Author",author=a,form=f)

@app.route("/edit/new_author", methods=("POST", "GET", ))
def edit_new_author():
    a = None
    f = AuthorForm()
    if f.validate_on_submit():
        if NotOnDB(f.name.data):
            a = Author(name = f.name.data)
            db.session.add(a)
            db.session.commit()
            return redirect(url_for("one_author", id = a.id))
    return render_template(
        "edit-new-author.html",
        form = f,
        author = a
    )

@app.route("/one_author/<int:id>")
def one_author(id):
    a = get_author(id)
    return render_template("one-author.html",
    title="Affichage Author ",
    author=a)
    
@app.route("/save/author/", methods=("POST", ))
def save_author():
    a = None
    f = AuthorForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_author(id)
        a.name = f.name.data
        db.session.commit()
        return redirect(url_for("one_author", id = id))
    a = get_author(int(f.id.data))
    return render_template(
        "edit-author.html",
        author = a,
        form = f
    )

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        user = get_user(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None


@app.route("/login/", methods=("GET", "POST",))
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            next = f.next.data or url_for("home")
            login_user(user)
            return redirect(next)
    return render_template(
        "login.html",
        form = f
    )

@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for("home"))
